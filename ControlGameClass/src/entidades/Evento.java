package entidades;

import java.util.Date;
import java.util.List;

public class Evento {
    int id;
    String nome;
    Date data;
    int tipo;
    /**
     * 1 - Torneio
     * 2 - Treino
     * */
    List<Etapa> etapas;
    List<JogadorEtapa> jogadores;
    List<Jogo> jogos;
}
