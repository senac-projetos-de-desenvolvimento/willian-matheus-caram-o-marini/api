package entidades;

import java.sql.Time;

public class Stats {
    int idJogo;
    int idJogadorJogo;
    int pontos;
    int rebotes;
    int assistencias;
    int bloqueios;
    int roubos;
    int erros;
    int twoPtsShot;
    int threePtsShot;
    int twoPtsMiss;
    int threePtsMiss;
    Time tempoEmQuadra; //tempo em quadra
}
