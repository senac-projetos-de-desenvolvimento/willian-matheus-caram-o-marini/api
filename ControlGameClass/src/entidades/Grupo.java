package entidades;

import java.util.Date;
import java.util.List;

public class Grupo {
    int id;
    String nome;
    Date criação;
    Usuario administrador;
    Usuario treinador;
    List<Usuario> atletas;
    List<Evento> eventos;
}
